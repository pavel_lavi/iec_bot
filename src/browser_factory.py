from selenium.webdriver.firefox.service import Service
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium import webdriver
from pyvirtualdisplay import Display
from constants import Constants


class BrowserFactory:
    def get_browser(self, os):
        return {
            Constants.WIN_CONTEXT: self._win_browser,
            Constants.FIREFOX_CONTEXT: self._firefox_browser,
            Constants.CHROME_CONTEXT: self._chrome_browser
        }.get(os, Constants.CHROME_CONTEXT)()

    @staticmethod
    def _win_browser():
        firefox_options = Options()
        firefox_options.headless = False
        service = Service(Constants.FIREFOX_WIN_BIN_PATH, log_path=Constants.DRIVER_LOG_FILE_PATH)
        return webdriver.Firefox(service=service, options=firefox_options)

        #chrome_options = ChromeOptions()
        #chrome_options.binary_location = 'C:\Program Files\Google\Chrome\Application\chrome.exe'
        #return webdriver.Chrome(
        #    executable_path='chromedriver',
        #    options=chrome_options,
        #    service_log_path=Constants.DRIVER_LOG_FILE_PATH)

    @staticmethod
    def _firefox_browser():
        display = Display(visible=False, size=(1920, 1080))
        print('display start')
        display.start()
        print('display started')
        firefox_options = Options()
        firefox_options.headless = True
        firefox_options.set_preference("gfx.webrender.all", True)
        print('create service')
        service = Service(Constants.FIREFOX_BIN_PATH, log_path=Constants.DRIVER_LOG_FILE_PATH)
        print('return webdriver')
        return webdriver.Firefox(service=service, options=firefox_options)

    @staticmethod
    def _chrome_browser():
        chrome_options = ChromeOptions()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-tools")
        chrome_options.add_argument("--no-zygote")
        chrome_options.add_argument("--single-process")
        chrome_options.binary_location = '/opt/chromium/chrome'
        return webdriver.Chrome(
            executable_path='/opt/chromedriver/chromedriver_linux64/chromedriver',
            options=chrome_options,
            service_log_path=Constants.DRIVER_LOG_FILE_PATH)