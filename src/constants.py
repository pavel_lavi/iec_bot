class Constants(object):
    WIN_CONTEXT = 'win'
    FIREFOX_CONTEXT = 'firefox'
    CHROME_CONTEXT = 'chrome'

    FIREFOX_WIN_BIN_PATH = "./geckodriver.exe"
    FIREFOX_BIN_PATH = "geckodriver"
#   todo: container run
    DRIVER_LOG_FILE_PATH = "/tmp/driver.log"
    PIC_PATH = '/tmp/exception_screenshot.png'
#   todo: local windows run
    #DRIVER_LOG_FILE_PATH = "./tmp/driver.log"
    #PIC_PATH = './tmp/exception_screenshot.png'

    ACCOUNT_NUMBER = '346981575'
    METER_SERIAL_NUMBER = '5011278'