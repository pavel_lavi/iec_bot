from iec_update import report_new_iec_reading
from constants import Constants


def get_usage():
    return "Please send me reading value of meter with serial number 94943. up to 5 digits only!"


def return_xml(message):
    return f'<?xml version="1.0" encoding="UTF-8"?><Response><Message><Body>{message}</Body></Message></Response>'


def read_log_file():
    file = open(Constants.DRIVER_LOG_FILE_PATH)
    return file.read()


def lambda_handler(event, context):
    try:
        print(f'running function with event: {event}')
        new_reading = event['Body']
        if new_reading.isdecimal() and len(new_reading) < 6:
            print(f'update iec reading with {new_reading}')
            result = report_new_iec_reading(
                Constants.FIREFOX_CONTEXT,
                Constants.ACCOUNT_NUMBER,
                Constants.METER_SERIAL_NUMBER,
                new_reading)

            return return_xml(f'{result}\nlog: {read_log_file()}')

        print("input is wrong.. return usage")
        return return_xml(get_usage())
    except Exception as ex:
        return return_xml(ex)
