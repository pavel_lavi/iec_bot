#!/usr/bin/env python
import time
import signal
from contextlib import contextmanager
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import browser_factory
from constants import Constants

MAIN_URL = "https://www.iec.co.il/meter-reading"
START_BUTTON_XPATH = '//*[@id="mat-dialog-0"]/app-meter-wizard/app-dialog-container/div/app-wizard-container/div/div/app-form-wizard/div/wizard-step[1]/div/app-meter-welcome/div/div[1]/div[1]/app-button/button'
CONTRACT_NUMBER_XPATH = '//*[@id="contractNumber"]'
DEVICE_SERIAL_XPATH = '//*[@id="deviceNumber"]'
SUBMIT_BUTTON_XPATH = '//*[@id="mat-dialog-0"]/app-meter-wizard/app-dialog-container/div/app-wizard-container/div/div/app-form-wizard/div/wizard-step[2]/div/app-meter-search/div/div/form/div[3]/app-button/button'
FAILED_TO_SUBMIT_XPATH = '/html/body/div/div[4]/div/mat-dialog-container/app-notification-screen/app-dialog-container/div/div/div/div/h2'
LAST_READING_XPATH = '/html/body/div/div[2]/div/mat-dialog-container/app-meter-wizard/app-dialog-container/div/app-wizard-container/div/div/app-form-wizard/div/wizard-step[3]/div/app-meter-reading/div/div[1]/span[2]'
NEW_READING_INPUT_XPATH = '//*[@id="meters"]'
SUBMIT_NEW_READING_BUTTON_XPATH = '/html/body/div/div[2]/div/mat-dialog-container/app-meter-wizard/app-dialog-container/div/app-wizard-container/div/div/app-form-wizard/div/wizard-step[3]/div/app-meter-reading/div/form/div[3]/app-button/button'
SUBMITTED_MSG_XPATH = '/html/body/div/div[2]/div/mat-dialog-container/app-meter-wizard/app-dialog-container/div/app-wizard-container/div/div/app-form-wizard/div/wizard-step[4]/div/app-meter-success/div[1]/h1/span[2]'


class TimeoutException(Exception): pass


@contextmanager
def time_limit(seconds):
    def signal_handler(signum, frame):
        raise TimeoutException("Timed out!")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(seconds)
    try:
        yield
    finally:
        signal.alarm(0)


def report_new_iec_reading(app_os, contract_number, device_serial, new_reading):
    try:
        print(f'running in {app_os} context')
        with time_limit(2):
            browser = browser_factory.BrowserFactory().get_browser(app_os)
    except Exception:
        return 'failed to init browser'

    try:
        print(f'trying to open {MAIN_URL}')
        browser.set_page_load_timeout(5)
        browser.get(MAIN_URL)

        # print('zoom out')
        # browser.execute_script("document.body.style.zoom='75%'")

        print('get start button')
        get_element_with_retry(browser, START_BUTTON_XPATH).click()
        print(f'Updating contract number with value: {contract_number}')
        get_element_with_retry(browser, CONTRACT_NUMBER_XPATH).send_keys(contract_number)
        print(f'Updating device serial number with value: {device_serial}')
        get_element_with_retry(browser, DEVICE_SERIAL_XPATH).send_keys(device_serial)
        print(f'get submit button')
        b = get_element_with_retry(browser, SUBMIT_BUTTON_XPATH)
        b.click()
        b.click()

        try:
            last_reading = get_element_with_retry(browser, LAST_READING_XPATH).text
            print(f'Last reading is: {last_reading}')
            print(f'Updating new reading with value: {new_reading}')
            get_element_with_retry(browser, NEW_READING_INPUT_XPATH).send_keys(new_reading)
            print(f'get submit reading button')
            b2 = get_element_with_retry(browser, SUBMIT_NEW_READING_BUTTON_XPATH)
            b2.click()
        except TimeoutException:
            get_element_with_retry(browser, FAILED_TO_SUBMIT_XPATH)
            return f"customer data was not found by iec, please double check conf"

        print('verify submitted successful message')
        try:
            get_element_with_retry(browser, SUBMITTED_MSG_XPATH)
        except TimeoutException:
            raise Exception("submitted verification message could not be located")
        return f"all done :)"
    except Exception as ex:
        print("take screenshot")
        browser.get_screenshot_as_file(Constants.PIC_PATH)
        return f'Iec update failed, error: {ex}'
    finally:
        browser.quit()


def get_element_with_retry(browser, xpath):
    return WebDriverWait(browser, 5).until(EC.visibility_of_element_located((By.XPATH, xpath)))


if __name__ == "__main__":
    # result = report_new_iec_reading(Constants.WIN_CONTEXT, Constants.ACCOUNT_NUMBER, Constants.METER_SERIAL_NUMBER, '94943')
    result = report_new_iec_reading(Constants.FIREFOX_CONTEXT, Constants.ACCOUNT_NUMBER, Constants.METER_SERIAL_NUMBER, '94943')
    print(f'{result} \n log: {open(Constants.DRIVER_LOG_FILE_PATH).readlines()}')
