import json
import pytest
from src import app


@pytest.fixture()
def apigw_event():
    return {
    "SmsMessageSid": "SM1c20489f8c64f26de4dde7d17e4c4b57",
    "NumMedia": "0",
    "ProfileName": "Pavel",
    "SmsSid": "SM1c20489f8c64f26de4dde7d17e4c4b57",
    "WaId": "972545314997",
    "SmsStatus": "received",
    "Body": "44533",
    "To": "whatsapp%3A%2B18583775394",
    "NumSegments": "1",
    "ReferralNumMedia": "0",
    "MessageSid": "SM1c20489f8c64f26de4dde7d17e4c4b57",
    "AccountSid": "AC6e954159f4f80d963beba0f89f0d7e6f",
    "From": "whatsapp%3A%2B972545314997",
    "ApiVersion": "2010-04-01"
    }


def test_lambda_handler(apigw_event):
    ret = app.lambda_handler(apigw_event, "")
    print(f"POOP: {ret}")

    assert False